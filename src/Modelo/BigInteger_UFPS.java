/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.util.Arrays;

/**
 *
 * @author madar
 */
public class BigInteger_UFPS {

    /**
     * Numero="23456" miNUmero={2,3,4,5,6};
     */
    private int miNumero[];

    //Constructores
    public BigInteger_UFPS() {
    }

    /**
    *  Convierte la cadena miNumero en una lista con elementos numericos de esa cadena
    * 
     * @param miNumero
     * @throws java.lang.Exception
    */
    public BigInteger_UFPS(String miNumero) throws Exception {
//        char[] bit = miNumero.toCharArray();
//        this.miNumero = new int[bit.length];
//        
//        for (int i = 0; i < bit.length; i++){
//            if(miNumero.charAt(i)>'0' || miNumero.charAt(i)<'9')
//                  throw new Exception("Solo puede ingresar números");
//                
//            this.miNumero[i] = bit[i] - '0';
//        }
        this.miNumero = new int[miNumero.length()];
        for (int i = 0; i < miNumero.length(); i++) {
            this.miNumero[i] = Integer.parseInt(String.valueOf(miNumero.charAt(i)));
        }
    }
    

    //Getter y Setter
    public int[] getMiNumero() {
        return miNumero;
    }

    public void setMiNumero(int[] miNumero) {
        this.miNumero = miNumero;
    }

    /**
     * Mutiplica dos enteros BigInteger
     * 
     * @param dos
     * @return 
     * @throws java.lang.Exception 
     */
    public BigInteger_UFPS multiply(BigInteger_UFPS dos) throws Exception {
    /* El resultado se debe guardar como un vector de n posiciones separado por "," */
        String tmp = Prod_Str(dos);
        
        BigInteger_UFPS prd = new BigInteger_UFPS(tmp);
        return prd;
    }    
    
    /**
     * 
     * @param new_val
     * @return 
     */
    private String Prod_Str(BigInteger_UFPS new_val) {        
        String str = "";
        int sum = 0, prt;
        
        for (int i = this.getMiNumero().length-1; i > -1; i--) {
            prt = new_val.intValue() * this.getMiNumero()[i];
            prt += sum;
            sum = 0;
            str += String.valueOf(prt%10);
            
            if(prt > 10){
                sum = prt/10;
                if(i == 0)
                    if(sum >= 10)
                        str += raetlov(String.valueOf(sum));
                    else
                        str += sum; 
            }
        }
        
        return raetlov(str);
    }

    /**
     * Voltea el texto enviado
     * 
     * @param txt
     * @return retorna la cadena invertida
     */
    private String raetlov(String txt) {
        String str = "";
        
        for (int i = txt.length()-1; i > -1; i--)
            str += txt.charAt(i);
        
        return str;
    }
    
    /**
     * Retorna la representación entera del BigInteger_UFPS
     * @return un entero
     */
    public int intValue() {
        return Integer.parseInt(toString());
    }
        
    /**
    * @return Valor de la variable miNumero en String
    */
    @Override
    public String toString() {
        String str = "";
        
        for (int i : this.miNumero)
            str += String.valueOf(i);
        
        return str;
    }

//    
//    public String[] separa(int n) {
//        
//        String separarString = ",";
//        String separar [] = null;
//        
//        for (int i = 0; i < tamanioNum(n).length() ; i++) {
//            separar[i] += primerDigito(n) + separarString;
//        }
//        
//        return separar;
//    }
//    
//    private int primerDigito(int n) {
//
//        int aux = 0;
//
//        for (int i = 0; i < tamanioNum(n).length(); i++) {
//            if (tamanioNum(n).length() > 1) {
//                n = n / 10;
//                if (n <= 1) {
//                    break;
//                }
//            } else if (tamanioNum(n).length() == 1) {
//                aux = n;
//            }
//        }
//
//        return aux;
//    }
//    
//    private String tamanioNum (int n) {
//        String cad = Integer.toString(n);
//        return cad;
//    }
}
